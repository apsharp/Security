package com.dibdibs.security;

import java.util.Random;

public class Util {
	public static final int RANDOMSTRING_ALPHA = 0;
	public static final int RANDOMSTRING_NUMERIC = 1;
	public static final int RANDOMSTRING_ALPHANUMERIC = 2;
	public static final int RANDOMSTRING_HEX = 3;
	
	public static int randomInteger(int min, int max){
		max += 1;
		
		return new Random().nextInt(max - min) + min;
	}
	
	public static String randomString(int type, int len){
		String charset = "";
		StringBuilder resultBuilder = new StringBuilder();
		
		if (type == Util.RANDOMSTRING_ALPHA){
			charset = "abcdefghijklmnopqrstuvwxyz";
		}
		
		else if (type == Util.RANDOMSTRING_NUMERIC){
			charset = "1234567890";
		}
		
		else if (type == Util.RANDOMSTRING_HEX){
			charset = "0123456789abcdef";
		}
		
		else {
			charset = "abcdefghijklmnopqrstuvwxyz1234567890";
		}
		
		for (int i = 0; i < len; i++){
			resultBuilder.append(charset.charAt(Util.randomInteger(0, charset.length() - 1)));
		}
		
		return resultBuilder.toString();
	}
}
