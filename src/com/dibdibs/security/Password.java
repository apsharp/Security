package com.dibdibs.security;

public class Password {
	public static final int PASSWD_ALGO1 = 1;
	
	public String raw;
	public String secured;
	
	public int algo;
	public int iterations = 0;
	public String firstSalt;
	public String secondSalt;
	
	
	@Override
	public boolean equals(Object password){
		if(!(password instanceof Password)){
			return false;
		}
		
		Password passwdObj = (Password) password;
		
		if (this.secured == passwdObj.secured){
			return true;
		}
		
		else{
			return false;
		}
	}
	
	public String secure(int algo){
		switch (algo){
			case (Password.PASSWD_ALGO1):
			default:
				this.secured = this.algoOne();
				return this.secured;
		}
	}
	
	public String secure(){
		return this.secure(this.algo);
	}
	
	private String algoOne(){
		//Decide on Iterations.
		if (this.iterations == 0){
			this.iterations = Util.randomInteger(1500, 7500);
		}
		
		//Salt
		if (this.firstSalt == null){
			int fSaltLen = Util.randomInteger(512, 2048);
			this.firstSalt = Util.randomString(Util.RANDOMSTRING_HEX, fSaltLen);
		}
		
		if (this.secondSalt == null){
			int sSaltLen = Util.randomInteger(256, 4096);
			this.firstSalt = Util.randomString(Util.RANDOMSTRING_HEX, sSaltLen);
		}
		
		//Creating local variable so class variable is not changed a lot.
		//No efficiency / programming reason for it, just feels better :D
		String secured = this.firstSalt + this.raw + this.secondSalt;
		
		for (int i = 0; i < this.iterations; i++){
			secured = Hash.passwdHash(secured, "MD5");
			secured = Hash.passwdHash(secured, "SHA-512");
		}
		
		secured = this.firstSalt + secured + this.secondSalt;
		
		this.secured = secured;
		return secured;
	}
}