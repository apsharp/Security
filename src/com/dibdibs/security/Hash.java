package com.dibdibs.security;

import java.security.MessageDigest;

public class Hash {
	public static String passwdHash(String raw, String algo){
		try {
			MessageDigest digest = MessageDigest.getInstance(algo);
			
			byte[] bytes = raw.getBytes();
			
			byte[] hashedBytes = digest.digest(bytes);
			
			StringBuffer sBuffer = new StringBuffer();
			
			for (int i = 0; i < hashedBytes.length; i++){
				sBuffer.append(Integer.toString((hashedBytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			
			return sBuffer.toString();
		}
		
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
